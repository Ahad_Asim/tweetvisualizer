package com.company.tweetvisualizer.models

data class Tweet(val id: String , val userName: String  ,val tweet: String)
