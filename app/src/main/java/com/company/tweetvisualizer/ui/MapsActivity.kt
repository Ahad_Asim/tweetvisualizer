package com.company.tweetvisualizer.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.company.tweetvisualizer.R
import com.company.tweetvisualizer.databinding.ActivityMapsBinding
import com.company.tweetvisualizer.models.MarkerInfo
import com.company.tweetvisualizer.network.RetrofitHelper
import com.company.tweetvisualizer.network.TwiterAPI
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MapsActivity : AppCompatActivity(), OnMapReadyCallback  {

    val PERMISSION_ID = 7
    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private var mCurrentLatitude: Double = 0.0
    private var mCurrentLongitude: Double = 0.0

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation: Location = locationResult.lastLocation
            mCurrentLatitude = mLastLocation.latitude
            mCurrentLongitude = mLastLocation.longitude

            Log.d("Location: " , mCurrentLatitude.toString())
            Log.d("Location: " , mCurrentLongitude.toString())

            focusOnCurrentLocation()

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        if (!checkPermissions()) {
            requestPermissions()
            return
        }else{
            if (isLocationEnabled()){
                requestLocation()
            }else{
                Toast.makeText(this , "Location disabled" ,Toast.LENGTH_LONG).show()
            }
        }

    }
    private fun requestLocation() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions()
            return
        }
        mFusedLocationClient.lastLocation
            .addOnSuccessListener { location : Location? ->
                // Got last known location. In some rare situations this can be null.
                if (location != null) {
                    mCurrentLatitude = location.latitude
                    mCurrentLongitude = location.longitude

                    Log.d("Location: " , mCurrentLatitude.toString());
                    Log.d("Location: " , mCurrentLongitude.toString());
                    focusOnCurrentLocation()
                }else{
                    requestNewLocationData()
                }
            }
    }

    private fun focusOnCurrentLocation(){
        // Add a marker in Sydney and move the camera
        val currentLoction = LatLng(mCurrentLatitude, mCurrentLongitude)

        var markerInfo = MarkerInfo("TW1212" , "Ahad" , "Some text in tweet");
        val gson = Gson()
        val markerInfoString: String = gson.toJson(markerInfo)

        mMap.addMarker(MarkerOptions().position(currentLoction).title(markerInfo.userName).snippet(markerInfoString))
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLoction , 15f))

        val twitterApi = RetrofitHelper.getInstance().create(TwiterAPI::class.java)
        // launching a new coroutine
        GlobalScope.launch {
            val result = twitterApi.getTweets()
            if (result != null){

            }
        }
    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            return true
        }
        return false
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
            PERMISSION_ID
        )
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient!!.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private fun isLocationEnabled(): Boolean {
        var locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setInfoWindowAdapter(CustomInfoWindowAdapter(this))
        mMap.setOnInfoWindowClickListener {
            val gson = Gson()
            val markerInfo = gson.fromJson(it.snippet, MarkerInfo::class.java)
            var intent = Intent(this , TweetDetailActivity::class.java)
            intent.putExtra("data" , it.snippet)
            startActivity(intent)

        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                if (isLocationEnabled()){
                    requestLocation()
                }else{
                    Toast.makeText(this , "Location disabled" ,Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}