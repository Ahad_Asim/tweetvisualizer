package com.company.tweetvisualizer.ui

import android.app.Activity
import android.content.Context
import android.view.View
import android.widget.TextView
import com.company.tweetvisualizer.R
import com.company.tweetvisualizer.models.MarkerInfo
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.google.gson.Gson


class CustomInfoWindowAdapter (context: Context) : GoogleMap.InfoWindowAdapter {

    var mContext = context
    var mWindow = (context as Activity).layoutInflater.inflate(R.layout.custom_info_window, null)

    private fun rendowWindowText(marker: Marker, view: View){

        val tvTitle = view.findViewById<TextView>(R.id.title)
        val tvSnippet = view.findViewById<TextView>(R.id.snippet)

        tvTitle.text = marker.title

        val gson = Gson()
        val markerInfo = gson.fromJson(marker.snippet, MarkerInfo::class.java)
        tvSnippet.text = markerInfo.tweet

    }

    override fun getInfoContents(marker: Marker): View {
        rendowWindowText(marker, mWindow)
        return mWindow
    }

    override fun getInfoWindow(marker: Marker): View? {
        rendowWindowText(marker, mWindow)
        return mWindow
    }
}
