package com.company.tweetvisualizer.network

import com.company.tweetvisualizer.models.Tweet
import okhttp3.Response

data class TweetAPIResponse(
    val count: Int,
    val lastItemIndex: Int,
    val page: Int,
    val results: List<Tweet>,
    val totalCount: Int,
    val totalPages: Int
)