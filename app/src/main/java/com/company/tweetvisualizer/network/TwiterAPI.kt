package com.company.tweetvisualizer.network

import retrofit2.Call
import retrofit2.http.GET

interface TwiterAPI {

    @GET("search/tweets.json")
    suspend fun getTweets() : Call<TweetAPIResponse>

}